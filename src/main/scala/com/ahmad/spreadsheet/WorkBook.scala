package com.ahmad.spreadsheet


case class WorkBook(rows: Int,
                    columns: Int,
                    cellMatrix: Array[Array[Cell]])
