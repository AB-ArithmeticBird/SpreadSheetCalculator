package com.ahmad.spreadsheet


object Expression {

  //Algebraic Data type for Expression tree

  sealed trait ExpressionTree

  sealed trait ArithmeticExpression extends ExpressionTree

  //example A1, A456
  case class CoOrdinate(row: Int, column: Int) extends ArithmeticExpression

  // + operator
  case class Add(e1: ArithmeticExpression, e2: ArithmeticExpression) extends ArithmeticExpression

  // - operator
  case class Sub(e1: ArithmeticExpression, e2: ArithmeticExpression) extends ArithmeticExpression


  // * operator
  case class Mul(e1: ArithmeticExpression, e2: ArithmeticExpression) extends ArithmeticExpression


  // / operator
  case class Div(e1: ArithmeticExpression, e2: ArithmeticExpression) extends ArithmeticExpression

  // A number
  case class Numeric(e: Double) extends ArithmeticExpression


  // An error
  case class ExpressionError(msg: String) extends ExpressionTree

  //A numeric which stays independent and do not come after "="
  case class FreeNumeric (e:Double) extends ExpressionTree


  //Name for a Column
  case class Tag(value: String) extends ExpressionTree

  // Blank
  case object Blank extends ExpressionTree



  type Spreadsheet =  Array[Array[Cell]]

  def eval(e:ArithmeticExpression) (implicit spreadSheet: Spreadsheet):Option[Double] = e match {
    case CoOrdinate(row, column) =>
      if ((row > spreadSheet.length)||(column > spreadSheet(0).length))
        None
      else
        spreadSheet(row-1)(column-1).value(spreadSheet)

    case Add(e1, e2) =>
      eval(e1).flatMap( ee1 =>  eval(e2).map(ee2 => ee1 + ee2))

    case Sub(e1, e2) =>
      eval(e1).flatMap( ee1 =>  eval(e2).map(ee2 => ee1 - ee2))

    case Mul(e1, e2) =>
      eval(e1).flatMap( ee1 =>  eval(e2).map(ee2 => ee1 * ee2))
    case Div(e1, e2) =>
      eval(e1).flatMap( ee1 =>  eval(e2).map(ee2 => ee1 / ee2))

    case Numeric(x) => Some(x)

  }

  def eval(e: ExpressionTree)(implicit spreadSheet: Spreadsheet): String = e match {
    case a:ArithmeticExpression => eval(a)(spreadSheet).toString

    case ExpressionError(msg) =>  msg.toString

    case Blank => " "

    case Tag(v) => v

    case FreeNumeric(v) => v.toString
  }

}

