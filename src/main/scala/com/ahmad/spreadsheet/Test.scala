package com.ahmad.spreadsheet


object Test extends App {

  val x: Option[WorkBook] = WorkBookModule.create()

  x.foreach {  w => w.cellMatrix foreach { row =>
    row foreach { r =>
      print (r + "\t")
    }
    println
  }
    println("SOLVED:")

    x.foreach{ y =>
      WorkBookModule.evaluate(y)
    }

  }
}



