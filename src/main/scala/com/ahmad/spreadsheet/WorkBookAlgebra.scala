package com.ahmad.spreadsheet


trait WorkBookAlgebra{
  def create(): Option[WorkBook]
  def evaluate (workBook: WorkBook)
}
