package com.ahmad.spreadsheet

import com.ahmad.spreadsheet.Expression._

import scala.util.parsing.combinator.{JavaTokenParsers, RegexParsers}

/*
 * grammar for expression
 */
trait ExpressionParser extends JavaTokenParsers with RegexParsers {

  lazy val arithexpr: Parser[ArithmeticExpression] = term ~ rep("[+-]".r ~ term) ^^ {
    case e ~ es => es.foldLeft(e) {
      case (e1, "+" ~ e2) => Add(e1, e2)
      case (e1, "-" ~ e2) => Sub(e1, e2)
    }
  }

  lazy val term = factor ~ rep("[*/]".r ~ factor) ^^ {
    case t ~ ts => ts.foldLeft(t) {
      case (e1, "*" ~ e2) => Mul(e1, e2)
      case (e1, "/" ~ e2) => Div(e1, e2)
    }
  }

  lazy val factor = "(" ~> arithexpr <~ ")" | num | cell


  lazy val num = floatingPointNumber ^^ { e =>
    Numeric(e.toDouble)
  }

  lazy val cell: Parser[CoOrdinate] =
    """[A-Za-z]+\d\d*""".r ^^ { s =>

      val (columnName: String, rowName: String) = getColumnAndRow(s)

      val column: Int = columnName.iterator.map { c => c - 'A' + 1 }.toList.reverse
        .zipWithIndex.map { case (v, i) => v * math.pow(26, i).toInt }.sum
      val row = rowName.toInt

      CoOrdinate(row, column)
    }

  lazy val freenum = """^-?\d+(,\d+)*(\.\d+(e\d+)?)?$""".r ^^ { e =>
    FreeNumeric(e.toDouble)
  }

  lazy val tag: Parser[Tag] =
    """[^=].*""".r ^^ Tag

  lazy val expr: Parser[ExpressionTree] =
    freenum | tag | "=" ~> arithexpr

  def parse(input: String): ExpressionTree =
    parseAll(expr, input) match {
      case Success(e, _) => e
      case f: NoSuccess => Tag("[\"Error!\"]")
    }

  private def getColumnAndRow(str: String): (String, String) = {
    def s(it: Iterator[Char], n: Int): Option[Int] = {
      if (it.hasNext) {
        val x = it.next()
        if (x.isDigit) Some(n) else s(it, n + 1)
      }
      else None
    }
    val index: Option[Int] = s(str.iterator, 0)
    index match {
      case Some(x) => (str.take(x), str.substring(x))
      case None => sys.error("It should not happen")
    }
  }
}
