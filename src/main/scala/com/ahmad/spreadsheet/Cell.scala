package com.ahmad.spreadsheet

import com.ahmad.spreadsheet.Expression.{ArithmeticExpression, ExpressionTree, FreeNumeric, Spreadsheet}

case class Cell(row:Int, column:Int, formula: String) extends ExpressionParser{

  def value(spreadSheet: Spreadsheet): Option[Double] ={
    val r: ExpressionTree = parse(formula)
    r match {
           case aex:ArithmeticExpression => Expression.eval(aex)(spreadSheet)
           case FreeNumeric(x) => Some(x)
           case _ => Some(0.0)
         }
  }

  def toString(spreadSheet: Spreadsheet):String = parse(formula) match {
    case aex:ArithmeticExpression => Expression.eval(aex)(spreadSheet) match {
      case Some(s) => s.toString
      case None => "#Error!"
    }
    case _ @x =>Expression.eval(x)(spreadSheet)
  }

}

