package com.ahmad.spreadsheet


trait WorkBookModuleInterpreter extends WorkBookAlgebra{

  def create(): Option[WorkBook] = {
    val dimension = getSize
    if (dimension.isEmpty)  System.exit(0)
    val sheet: Option[WorkBook] = dimension.map{ d =>
      val spreadSheet: Array[Array[Cell]] =  Array.ofDim[Cell](d._2, d._1)     //r,c
      for (i <- 0 until d._2; j <- 0 until d._1) {
        val exp = Console.readLine().trim.toUpperCase
        spreadSheet(i)(j) = Cell(i, j, exp)
      }
      WorkBook(d._2,d._1, spreadSheet)
    }
    sheet
  }
  def evaluate (workBook: WorkBook) = {
    workBook.cellMatrix.foreach{ row =>
      row foreach { r => print (r.toString(workBook.cellMatrix) + "\t")}
      println
    }
  }
  private def getSize: Option[(Int,Int)] = {
    val sizeOfMatrix = Console.readLine()
    val s: Array[String] = sizeOfMatrix.split(" ")
    if (s.length != 2){
      warning()
    }
    else {
      val row: Option[Int] = toInt(s(0))
      val col: Option[Int] = toInt(s(1))
      if (row.isEmpty||col.isEmpty) {
        warning()
      }
      else
        row.flatMap(r => col.map (c =>(c,r)))
    }
  }

  private def warning(): Option[(Int, Int)] = {
    Console.println("Wrong number of columns and rows! Do you want to continue?(y/)")
    val answer = Console.readLine()
    if (answer.toUpperCase == "Y")
      getSize
    else
      None

  }

  private def toInt(s: String): Option[Int] = {
    try {
      Some(s.toInt)
    } catch {
      case e: Exception => None
    }
  }

}

object WorkBookModule extends  WorkBookModuleInterpreter
