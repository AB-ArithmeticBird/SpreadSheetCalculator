name := "Ahmad-Calc"

version := "1.0"

scalaVersion := "2.11.7"

resolvers += "Maven Central Server" at "http://repo1.maven.org/maven2"

libraryDependencies ++= Seq(
  //"org.scalatest" %% "scalatest" % "2.2.0" % "test" withSources(),
  "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.4" withSources()
)